﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using CRM.WebApp.Models.Applications;

namespace CRM.WebApp.Models.Clients
{
    public class Person : IClient
    {
        public Person()
        {
            ServiceRequests = new List<ServiceRequest>();
            Complaints = new List<Complaint>();
        }

        [IncomparableField]
        public int PersonId { get; set; }

        [StringLength(100, MinimumLength = 3)]
        [Required]
        [DisplayName("Imię")]
        public string Name { get; set; }

        [StringLength(100, MinimumLength = 3)]
        [Required]
        [DisplayName("Nazwisko")]
        public string Surname { get; set; }

        [IncomparableField]
        public virtual ContactDetails ContactDetails { get; set; }

        [IncomparableField]
        public virtual Address ClientAddress { get; set; }

        [IncomparableField]
        public virtual ICollection<ServiceRequest> ServiceRequests { get; set; }

        [IncomparableField]
        public virtual ICollection<Complaint> Complaints { get; set; }

        #region IClient members

        const string prefix = "p";

        string IClient.ClientId
        {
            get
            {
                return prefix + PersonId;
            }
            set
            {
                PersonId = Int16.Parse(value.Substring(prefix.Length));
            }
        }

        string IClient.Name
        {
            get
            {
                return string.Format("{0} {1}", Name, Surname);
            }
        }

        ICollection<ServiceRequest> IClient.ServiceRequests
        {
            get
            {
                return ServiceRequests;
            }
            set
            {
                ServiceRequests = value;
            }
        }

        ICollection<Complaint> IClient.Complaints
        {
            get
            {
                return Complaints;
            }
            set
            {
                Complaints = value;
            }
        }
        #endregion
    }
}