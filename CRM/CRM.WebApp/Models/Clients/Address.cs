﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CRM.WebApp.Models.Clients
{
    public class Address
    {
        public int AddressId { get; set; }

        [StringLength(100, MinimumLength = 3)]
        [Required(ErrorMessage = "Commune Name is required")]
        [DisplayName("Gmina")]
        public string Community { get; set; } //gmina - jak ktoś ma lepsze tłumaczenie to niech da znać

        [StringLength(6)]
        [Required(ErrorMessage = "Postal Code Name is required")]
        [DisplayName("Kod pocztowy")]
        public string PostalCode { get; set; }

        [StringLength(100, MinimumLength = 2)]
        [Required(ErrorMessage = "City Name is required")]
        [DisplayName("Miasto")]
        public string City { get; set; }

        [StringLength(100, MinimumLength = 1)]
        [DisplayName("Numer budynku")]
        public string BuildingNr { get; set; } //nr budynku

        [StringLength(100, MinimumLength = 1)]
        [DisplayName("Numer mieszkania")]
        public string ApartmentNr { get; set; } //nr mieszkania

        [StringLength(100, MinimumLength = 3)]
        [Required(ErrorMessage = "District Name is required")]
        [DisplayName("Powiat")]
        public string District { get; set; } //powiat

        [StringLength(100, MinimumLength = 3)]
        [Required(ErrorMessage = "Street Name is required")]
        [DisplayName("Ulica")]
        public string Street{ get; set; }

        [StringLength(100, MinimumLength = 3)]
        [Required]
        [DisplayName("Województwo")]
        public string Province { get; set; } //województwo
    }
}