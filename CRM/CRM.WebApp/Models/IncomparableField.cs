﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CRM.WebApp.Models
{
    /// <summary>
    /// This class is used to mark fields which should be skipped in the process of comparing two objects by their fields content.
    /// </summary>
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field)]
    public class IncomparableField : Attribute { }
}