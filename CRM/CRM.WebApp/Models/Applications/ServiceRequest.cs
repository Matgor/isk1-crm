﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using CRM.WebApp.Models.Applications.Services;
using System.ComponentModel.DataAnnotations.Schema;
using CRM.WebApp.Models.Applications.History;

namespace CRM.WebApp.Models.Applications
{
    public interface IServiceRequest
    {
        int ServiceRequestId { get; set; }
        string Title { get; set; }
        DateTime? CreationTime { get; set; }
        DateTime? EndTime { get; set; }
        DateTime? PredictedRealizationDate { get; set; }
        string ClientInterview { get; set; }
        ServiceRequestStatus Status { get; set; }
        IList<Document> Documents { get; set; }
        Analyze Analyze { get; set; }
        Maintenance Maintenance { get; set; }
        Repair Repair { get; set; }
        Replacement Replacement { get; set; }
        RequestHistory History { get; set; }
    }

    public enum ServiceRequestStatus
    {
        Await = 0, InProgress = 1, Completed = 2
    }

    public class ServiceRequest : IServiceRequest
    {
        /// <summary>
        /// Id
        /// </summary>
        [Key]
        [Required]
        [DisplayName("Id")]
        public int ServiceRequestId { get; set; }

        /// <summary>
        /// Title
        /// </summary>
        [Required]
        [StringLength(100, MinimumLength = 3)]
        [DisplayName("Tytuł zgłoszenia")]
        public string Title { get; set; }
    
        /// <summary>
        /// Time when application been officialy started.
        /// </summary>
        [DisplayFormat(DataFormatString = "{0:dd MMM yyyy}")]
        [IncomparableField]
        public DateTime? CreationTime { get; set; }

        /// <summary>
        /// Time when application been officialy ended.
        /// </summary>
        [DisplayFormat(DataFormatString = "{0:dd MMM yyyy}")]
        public DateTime? EndTime { get; set; }

        /// <summary>
        /// Date until requested service task should be done.
        /// </summary>
        [Required]
        [DisplayName("Przewidywana data realizacji")]
        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd HH:mm}")]
        public DateTime? PredictedRealizationDate { get; set; }

        [StringLength(1000)]
        [DisplayName("Wywiad z klientem")]
        public string ClientInterview { get; set; }

        /// <summary>
        /// Converted to enum ServiceRequestStatus. This property should be used in logic layer. EF6 sucks in some ways.
        /// If ServiceRequestStatus is null, returns default value = "Await".
        /// </summary>
        [NotMapped]
        [DisplayName("Status")]
        public ServiceRequestStatus Status
        {
            get { return (ServiceRequestStatus)Enum.Parse(typeof(ServiceRequestStatus), !string.IsNullOrEmpty(ServiceRequestStatus) ? ServiceRequestStatus : CRM.WebApp.Models.Applications.ServiceRequestStatus.Await.ToString()); }
            set { ServiceRequestStatus = value.ToString(); }
        }

        [IncomparableField]
        [DisplayName("Załączniki")]
        public virtual IList<Document> Documents { get; set; }

        /// <summary>
        /// Used only for mapping. Converted to Status property;
        /// </summary>
        [IncomparableField]
        public string ServiceRequestStatus { get; set; } // Must be public!

        /// <summary>
        /// Analyze details.
        /// </summary>
        public virtual Analyze Analyze { get; set; }

        /// <summary>
        /// Mainatenance details.
        /// </summary>
        public virtual Maintenance Maintenance { get; set; }

        /// <summary>
        /// Repair details.
        /// </summary>
        public virtual Repair Repair { get; set; }

        /// <summary>
        /// Replacement details.
        /// </summary>
        public virtual Replacement Replacement { get; set; }

        /// <summary>
        /// Recovery details.
        /// </summary>
        public virtual Recovery Recovery { get; set; }

        /// <summary>
        /// History details.
        /// </summary>
        [IncomparableField]
        [DisplayName("Historia zgłoszenia")]
        public virtual RequestHistory History { get; set; }
    }
}