﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRM.WebApp.Models.Applications.History
{
    /// <summary>
    /// 
    /// </summary>
    public abstract class RequestHistoryItem
    {
        // Fields

        /// <summary>
        /// Id
        /// </summary>
        [DisplayName("Id")]
        public int RequestHistoryItemId { get; set; }

        /// <summary>
        /// Time when arg was created.
        /// </summary>
        [Required]
        [DisplayName("Czas")]
        public DateTime CreationTime { get; set; }

        // Methods

        /// <summary>
        /// Description of arg.
        /// </summary>
        /// <returns></returns>
        public abstract string GetDescription();
    }

    /// <summary>
    /// When request is created.
    /// </summary>
    public class RequestCreate : RequestHistoryItem
    {
        public override string GetDescription()
        {
            return string.Format("Stworzenie zgłoszenia.");
        }

        /// <summary>
        /// History arg containing information fields modifications done on request creation.
        /// </summary>
        public RequestHistoryItem ChangeHistoryItem { get; set; }
    }

    /// <summary>
    /// When request status has changed.
    /// </summary>
    public class RequestStatusChange : RequestHistoryItem
    {
        [Required]
        public string OldStatus { get; set; }

        [Required]
        public string NewStatus { get; set; }

        public override string GetDescription()
        {
            return string.Format("Zmiana statusu z {0} na {1}", OldStatus, NewStatus);
        }
    }

    /// <summary>
    /// When one of the fields has changed.
    /// </summary>
    public class RequestFieldChange : RequestHistoryItem
    {
        [Required]
        public string ChangedFieldName { get; set; }

        public override string GetDescription()
        {
            return string.Format("Zmiana szczegółów zgłoszenia na polu \"{0}\"", ChangedFieldName);
        }
    }

    public class RequestFieldsChange : RequestHistoryItem
    {
        public RequestFieldsChange()
        {
            ChangedFields = new List<RequestFieldChange>();
        }

        public List<RequestFieldChange> ChangedFields { get; set; }

        public override string GetDescription()
        {
            if (ChangedFields.Count == 0)
                return string.Format("Brak zmienionych pól.");

            // Prepare description
            string description = string.Empty;
            description += "Zmiana na polach: \"";
            foreach (RequestFieldChange field in ChangedFields)
                description += field.ChangedFieldName + "\", \"";

            // Cut of last comma sign.
            description = description.Substring(0, description.Length - 6);

            return description;
        }
    }

    /// <summary>
    /// When request is finished.3
    /// </summary>
    public class RequestFinish : RequestHistoryItem
    {
        public override string GetDescription()
        {
            return string.Format("Zamknięcie zgłoszenia.");
        }

        /// <summary>
        /// History arg containing information fields modifications done on request creation.
        /// </summary>
        public RequestHistoryItem ChangeHistoryItem { get; set; }
    }
}
