﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

using CRM.WebApp.Mixins;

namespace CRM.WebApp.Models.Applications.History
{
    public class RequestHistory
    {
        /// <summary>
        /// Constructor.
        /// This an constructor for both programmer and Entity framework.
        /// </summary>
        /// <param name="init">Should initialize variables?</param>
        public RequestHistory()
        {
            Items = new List<RequestHistoryItem>();
        }

        /// <summary>
        /// Id
        /// </summary>
        [Key]
        [DisplayName("Id")]
        public int RequestHistoryId { get; set; }

        /// <summary>
        /// History events of request
        /// </summary>
        [DisplayName("Zapisy")]
        public virtual List<RequestHistoryItem> Items { get; set; }
    }


    /// <summary>
    /// Extending methods for Request History method.
    /// </summary>
    public static class RequestHistoryExt
    {
        /// <summary>
        /// Adds to request history new history arg.
        /// </summary>
        /// <param name="request">Request.</param>
        /// <param name="createHistory">If history doesn't exist, </param>
        public static void AddHistoryItem(this ServiceRequest request, RequestHistoryItem item, bool createHistory = false)
        {
            if (request.IsNull())
                throw new NullReferenceException("Service request is null.");
            if (item.IsNull())
                throw new NullReferenceException("History item is null");

            // Prepare request history if necessery
            if (createHistory)
                if (request.History.IsNull())
                {
                    request.History = new RequestHistory();
                }

            // Adds arg to the history
            request.History.Items.Add(item);
        }
    }

}