﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace CRM.WebApp.Models.Applications
{
    public class Document
    {
        /// <summary>
        /// Id
        /// </summary>
        [DisplayName("Id")]
        public int DocumentId { get; set; }

        /// <summary>
        /// File Name
        /// </summary>
        [DisplayName("Nazwa Pliku")]
        public string FileName { get; set; }

        /// <summary>
        /// File Content
        /// </summary>
        [DisplayName("Zawartość")]
        public byte[] Data { get; set; }

        /// <summary>
        /// File extension
        /// </summary>
        [DisplayName("Rozszerzenie")]
        public string ContentType { get; set; }

        /// <summary>
        /// File Length
        /// </summary>
        [DisplayName("Długość")]
        public int ContentLength { get; set; }
    }
}