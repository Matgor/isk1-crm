﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace CRM.WebApp.Models.Applications
{
    public class Complaint
    {
        /// <summary>
        /// Id
        /// </summary>
        [DisplayName("Id")]
        public int ComplaintId { get; set; }

        /// <summary>
        /// Title
        /// </summary>
        [Required]
        [StringLength(100, MinimumLength=3)]
        [DisplayName("Tytuł")]
        public string Title { get; set; }

        /// <summary>
        /// Time when application been officialy started.
        /// </summary>
        [DisplayName("Czas powstania")]
        public DateTime? CreationTime { get; set; }

        /// <summary>
        /// Time when application been officialy ended.
        /// </summary>
        [DisplayName("Czas zakończenia")]
        public DateTime? EndTime { get; set; }

        [Required]
        [DisplayName("Opis")]
        public string Description { get; set; }

        /// <summary>
        /// Converted to enum ServiceRequestStatus. This property should be used in logic layer. EF6 sucks in some ways.
        /// </summary>
        [NotMapped]
        [DisplayName("Status")]
        public ComplaintStatus Status
        {
            get { return (ComplaintStatus)Enum.Parse(typeof(ComplaintStatus), !string.IsNullOrEmpty(ComplaintStatus) ? ComplaintStatus : CRM.WebApp.Models.Applications.ComplaintStatus.Await.ToString()); }
            set { ComplaintStatus = value.ToString(); }
        }

        /// <summary>
        /// Used only for mapping. Converted to Status property;
        /// </summary>
        public string ComplaintStatus { get; set; } // Must be public!
    }

    public enum ComplaintStatus
    {
        Canceled = 0, Await = 1, Solved = 2, InProgress = 3
    }
}