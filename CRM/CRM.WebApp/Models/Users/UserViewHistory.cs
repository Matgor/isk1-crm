﻿using CRM.WebApp.Models.Applications;
using CRM.WebApp.Models.Clients;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CRM.WebApp.Models.Users
{
    /// <summary>
    /// 
    /// </summary>
    public abstract class UserViewHistoryItem
    {
        [Key]
        public int UserViewHistoryItemID { get; set; }

        [Required]
        public DateTime LastTimeSeen { get; set; }
    }

    public class ServiceRequestViewHistoryItem : UserViewHistoryItem
    {
        public ServiceRequest ServiceRequest { get; set; }
    }

    public class PersonViewHistoryItem : UserViewHistoryItem
    {
        public Person Person { get; set; }
    }

    public class CompanyViewHistoryItem : UserViewHistoryItem
    {
        public Company Company { get; set; }
    }
}