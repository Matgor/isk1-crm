﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using CRM.WebApp.Models.Applications;
using CRM.WebApp.Models.Clients;

namespace CRM.WebApp.Models.Users
{
    public class User : IViewHistoryKeeper<ServiceRequestViewHistoryItem, ServiceRequest>, IViewHistoryKeeper<PersonViewHistoryItem, Person>, IViewHistoryKeeper<CompanyViewHistoryItem, Company>
    {
        public User()
        {
            ServiceReqestsViewHistory = new List<ServiceRequestViewHistoryItem>();
            PeopleViewHistory = new List<PersonViewHistoryItem>();
            CompaniesViewHistory = new List<CompanyViewHistoryItem>();
        }

        [Key]
        [Required]
        public int UserId { get; set; }

        [Required]
        [StringLength(25, MinimumLength = 5)]
        public string Login { get; set; }

        [Required]
        [StringLength(100)]
        public string Password { get; set; }

        [Required]
        public bool IsAdmin { get; set; }

        #region ===== IViewHistoryKeeper members =====

        public ICollection<ServiceRequestViewHistoryItem> ServiceReqestsViewHistory { get; set; }
        public virtual ICollection<PersonViewHistoryItem> PeopleViewHistory { get; set; }
        public virtual ICollection<CompanyViewHistoryItem> CompaniesViewHistory { get; set; }
        
        void IViewHistoryKeeper<ServiceRequestViewHistoryItem, ServiceRequest>.Update(ServiceRequest arg)
        {
            // 1. Check if arg exist in history
            // 2a. If not, create new history item and add it
            // 2b. If so, update its date.   

            // 1 - Check if arg exist in history
            var historyItem = ServiceReqestsViewHistory.FirstOrDefault(x => x.ServiceRequest.Equals(arg));

            if (historyItem == null) // historyItem is unpresent in collection
            {
                // 2a. - create new 
                historyItem = new ServiceRequestViewHistoryItem();
                historyItem.ServiceRequest = arg;
                historyItem.LastTimeSeen = DateTime.Now;
                ServiceReqestsViewHistory.Add(historyItem);
            }
            else // historyItem is present
            {
                // 2b - update date
                historyItem.LastTimeSeen = DateTime.Now;
            }
        }

        void IViewHistoryKeeper<PersonViewHistoryItem, Person>.Update(Person item)
        {
            // 1. Check if arg exist in history
            // 2a. If not, create new history item and add it
            // 2b. If so, update its date.   

            // 1 - Check if arg exist in history
            var historyItem = PeopleViewHistory.FirstOrDefault(x => x.Person.Equals(item));

            if (historyItem == null) // historyItem is unpresent in collection
            {
                // 2a. - create new 
                historyItem = new PersonViewHistoryItem();
                historyItem.Person = item;
                historyItem.LastTimeSeen = DateTime.Now;
                PeopleViewHistory.Add(historyItem);
            }
            else // historyItem is present
            {
                // 2b - update date
                historyItem.LastTimeSeen = DateTime.Now;
            }
        }

        void IViewHistoryKeeper<CompanyViewHistoryItem, Company>.Update(Company item)
        {
            // 1. Check if arg exist in history
            // 2a. If not, create new history item and add it
            // 2b. If so, update its date.   

            // 1 - Check if arg exist in history
            var historyItem = CompaniesViewHistory.FirstOrDefault(x => x.Company.Equals(item));

            if (historyItem == null) // historyItem is unpresent in collection
            {
                // 2a. - create new 
                historyItem = new CompanyViewHistoryItem();
                historyItem.Company = item;
                historyItem.LastTimeSeen = DateTime.Now;
                CompaniesViewHistory.Add(historyItem);
            }
            else // historyItem is present
            {
                // 2b - update date
                historyItem.LastTimeSeen = DateTime.Now;
            }
        }

        ICollection<ServiceRequestViewHistoryItem> IViewHistoryKeeper<ServiceRequestViewHistoryItem, ServiceRequest>.UserViewHistory
        {
            get { return ServiceReqestsViewHistory; }
        }

        ICollection<PersonViewHistoryItem> IViewHistoryKeeper<PersonViewHistoryItem, Person>.UserViewHistory
        {
            get { return PeopleViewHistory; }
        }

        ICollection<CompanyViewHistoryItem> IViewHistoryKeeper<CompanyViewHistoryItem, Company>.UserViewHistory
        {
            get { return CompaniesViewHistory; }
        }

        #endregion
    }
}