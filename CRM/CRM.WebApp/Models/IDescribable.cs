﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRM.WebApp.Models
{
    public interface IDescribable
    {
        string Description { get; set; }
    }
}
