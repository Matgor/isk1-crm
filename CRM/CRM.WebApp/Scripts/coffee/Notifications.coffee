﻿# CoffeeScript

#Funkcja wyświetlająca wszystkie notyfikacje
displayNotifications = () ->     
    wrapper = $("#notifications-wrapper")
    notifications = $(".notification")
    console.log(notifications.length)
    
    if wrapper.children.length > 0 and notifications.length > 0
        wrapper.show()
        for n in notifications
            do (n) ->
                n = $(n)
                n.on("click", () -> n.fadeOut(500);)
    else
        wrapper.hide()

$(document).ready(
    displayNotifications();
);