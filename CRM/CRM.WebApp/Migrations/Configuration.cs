namespace CRM.WebApp.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using CRM.WebApp.Models;

    using CRM.WebApp.Models.Applications;
    using CRM.WebApp.Models.Applications.Services;
    using System.Data.Entity.Infrastructure;
    using System.Data.Entity.Core.Metadata.Edm;
    using CRM.WebApp.Models.Users;
    internal sealed class Configuration : DbMigrationsConfiguration<CRM.WebApp.Models.CrmDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
            ContextKey = "CrmDbContext";
        }

        protected override void Seed(CrmDbContext context)
        {
            if (context.Users.Count() == 0)
            {
                var Users = new List<User>
                {
                    new User {Login = "admin", Password = "24a1ffa0ac930f2f3b75803897253b191df956ddcf2b3f44a582e77bfc5cfa8c", IsAdmin = true},
                };
                Users.ForEach(s => context.Users.AddOrUpdate(p => p.UserId, s));
            }

            if (context.ServiceRequests.Count() == 0 && false) // TODO remove this false1!!!!!!!!!!111111111111
            {
                // Add sample service requests
                var serviceRequets = SeedsGenerator.GenerateSampleServiceRequests();
                foreach (var request in serviceRequets)
                    context.ServiceRequests.Add(request);
            }

            if (context.People.Count() == 0)
            {
                // Add sample persons to database
                var persons = SeedsGenerator.GenerateSamplePersons();
                foreach (var person in persons)
                {
                    context.People.Add(person);
                }
            }

            if (context.Companies.Count() == 0)
            {
                //Add sample companies to database
                var companies = SeedsGenerator.GenerateSampleCompanies();
                foreach (var company in companies)
                {
                    context.Companies.Add(company);
                }
            }

            context.SaveChanges();
        }
    }
}
