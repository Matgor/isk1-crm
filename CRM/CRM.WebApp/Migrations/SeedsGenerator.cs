﻿using CRM.WebApp.Models.Applications;
using CRM.WebApp.Models.Clients;
using System;
using System.Collections.Generic;

namespace CRM.WebApp.Migrations
{
    internal sealed class SeedsGenerator
    {
        /// <summary>
        /// Generates 3 predifined sample service requests.
        /// </summary>
        /// <returns>ServiceRequests collection.</returns>
        public static IEnumerable<ServiceRequest> GenerateSampleServiceRequests()
        {
            #region request #0
            ServiceRequest r0 = new ServiceRequest()
            {
                Title = "Komputer się nie włącza",
                ClientInterview = "Po włączeniu komputer wydaje dziwne dźwięki i nie startuje. Cały czas czarny ekran.",
                CreationTime = DateTime.Now,
                Maintenance = new Models.Applications.Services.Maintenance() { Description = "Wyczyszczenie procesora z kurzu. Nałożenie pasty na procesor." },
                PredictedRealizationDate = new DateTime(DateTime.Now.Ticks + 1000 * 60 * 60 * 24 * 14),
                Repair = new Models.Applications.Services.Repair() { Description = "Naprawiono ścieżki na płycie głównej. Bla bla bla...." },
                Status = ServiceRequestStatus.Completed
            };
            #endregion

            #region request #1
            ServiceRequest r1 = new ServiceRequest()
            {
                Title = "Napęd nie chce odtwarzać płyt.",
                ClientInterview = "Napęd nie odtwarza żadnych płyt. Sprawdzano z CD i DVD.",
                CreationTime = new DateTime(2008, 11, 23, 11, 22, 33),
                PredictedRealizationDate = new DateTime(new DateTime(2008, 11, 23, 11, 22, 33).Ticks + 1000 * 60 * 60 * 24 * 14),
                Status = ServiceRequestStatus.Await
            };
            #endregion

            #region request #2
            ServiceRequest r2 = new ServiceRequest()
            {
                Title = "Drukarka nie chce drukować.",
                ClientInterview = "Przy próbie druku zapala się zółta lampka i hakuna matata.",
                CreationTime = new DateTime(2000, 1, 1, 3, 2, 13),
                PredictedRealizationDate = new DateTime(new DateTime(2000, 1, 1, 3, 2, 13).Ticks + 1000 * 60 * 60 * 24 * 10),
                Status = ServiceRequestStatus.Await
            };
            #endregion

            IEnumerable<ServiceRequest> requests = new List<ServiceRequest>()
            {
                r0, r1, r2
            };

            return requests;
        }
        public static IEnumerable<Person> GenerateSamplePersons()
        {
            #region address1
            Address address1 = new Address()
            {
                Community = "Warszawa",
                ApartmentNr = "3",
                BuildingNr = "3",
                City = "Warszawa",
                District = "Żoliborz",
                PostalCode = "01-789",
                Province = "Warszawa",
                Street = "Zajączka"
            };

            #endregion

            #region address2
            Address address2 = new Address()
            {
                Community = "Gdańsk",
                ApartmentNr = "5b",
                BuildingNr = "4",
                City = "Gdaańsk",
                District = "Morze",
                PostalCode = "04-576",
                Province = "Gdańsk",
                Street = "Główna"
            };
            #endregion

            #region address3
            Address address3 = new Address()
            {
                Community = "Dobre",
                ApartmentNr = "4",
                BuildingNr = "55",
                City = "Sworne Gacie",
                District = "Sworne Gacie",
                PostalCode = "08-234",
                Province = "Mazury",
                Street = "Kopciuszka"
            };
            #endregion

            #region contactDetails1
            ContactDetails details1 = new ContactDetails()
            {
                Email = "test@test.pl",
                PhoneNr = "88888888"
            };
            #endregion

            #region contactDetails2
            ContactDetails details2 = new ContactDetails()
            {
                Email = "mail@mail.pl",
                PhoneNr = "99999999"
            };
            #endregion

            #region contactDetails3
            ContactDetails details3 = new ContactDetails()
            {
                Email = "bla@bla.pl",
                PhoneNr = "88888888"
            };
            #endregion

            #region person1
            Person person1 = new Person()
            {
                Name = "Marcin",
                Surname = "Królewicz",
                ClientAddress = address1,
                ContactDetails = details1
            };
            #endregion

            #region person2
            Person person2 = new Person()
            {
                Name = "Barac",
                Surname = "Obama",
                ClientAddress = address2,
                ContactDetails = details2
            };
            #endregion

            #region person3
            Person person3 = new Person()
            {
                Name = "Nowakowa",
                Surname = "Dominika",
                ClientAddress = address3,
                ContactDetails = details3
            };
            #endregion

            IEnumerable<Person> persons = new List<Person>()
            {
                person1,person2,person3
            };

            return persons;
        }
        public static IEnumerable<Company> GenerateSampleCompanies()
        {
            #region address1
            Address address1 = new Address()
            {
                Community = "Warszawa",
                ApartmentNr = "3",
                BuildingNr = "3",
                City = "Warszawa",
                District = "Żoliborz",
                PostalCode = "01-789",
                Province = "Warszawa",
                Street = "Zajączka"
            };

            #endregion

            #region address2
            Address address2 = new Address()
            {
                Community = "Gdańsk",
                ApartmentNr = "5b",
                BuildingNr = "4",
                City = "Gdaańsk",
                District = "Morze",
                PostalCode = "04-576",
                Province = "Gdańsk",
                Street = "Główna"
            };
            #endregion

            #region address3
            Address address3 = new Address()
            {
                Community = "Zadupie",
                ApartmentNr = "4",
                BuildingNr = "55",
                City = "Zadupie",
                District = "Daleko",
                PostalCode = "54-039",
                Province = "Zadupie",
                Street = "Dupna"
            };
            #endregion

            #region contactDetails1
            ContactDetails details1 = new ContactDetails()
            {
                Email = "test@test.pl",
                PhoneNr = "99999999"
            };
            #endregion

            #region contactDetails2
            ContactDetails details2 = new ContactDetails()
            {
                Email = "dupa@dupa.pl",
                PhoneNr = "99999999"
            };
            #endregion

            #region contactDetails3
            ContactDetails details3 = new ContactDetails()
            {
                Email = "bla@bla.pl",
                PhoneNr = "99999999"
            };
            #endregion

            #region company1
            Company company1 = new Company()
            {
                CompanyName = "SuperFirma",
                NIP = "1234567890",
                ClientAddress = address1,
                ContactDetails = details1
            };
            #endregion
            #region company2
            Company company2 = new Company()
            {
                CompanyName = "KiepskaFrima",
                NIP = "0987654321",
                ClientAddress = address2,
                ContactDetails = details2
            };
            #endregion
            #region company3
            Company company3 = new Company()
            {
                CompanyName = "Krzak",
                NIP = "0385912395",
                ClientAddress = address3,
                ContactDetails = details3
            };
            #endregion
            IEnumerable<Company> companies = new List<Company>()
            {
                company1,company2,company3
            };

            return companies;
        }
    }
}