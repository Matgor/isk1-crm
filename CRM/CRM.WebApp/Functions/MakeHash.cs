﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CRM.WebApp.Models;
using System.Text;
using System.Security.Cryptography;

namespace CRM.WebApp.Functions
{
    public static class MakeHash
    {

        public static string ReturnHashPass(string UserPass)
        {
            UnicodeEncoding UE = new UnicodeEncoding();
            byte[] hashValue;
            byte[] message = UE.GetBytes(UserPass + "sooola");
            SHA256Managed hashString = new SHA256Managed();
            string hex = "";
            hashValue = hashString.ComputeHash(message);
            foreach (byte x in hashValue)
            {
                hex += String.Format("{0:x2}", x);
            }
            return hex;
        }
    }
}