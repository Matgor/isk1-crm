﻿using CRM.WebApp.Models;
using CRM.WebApp.Models.Appications;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace CRM.WebApp.Functions
{
    public class DocumentClass
    {
        public CrmDbContext db;

        public DocumentClass()
        {
            if (db == null)
                db = new CrmDbContext();
        }

        public void PostDocument(HttpPostedFileBase file)
        {
            if (file != null && file.ContentLength > 0)
            {
                var fileName = Path.GetFileName(file.FileName);
                var contentType = file.ContentType;
                var contentLength = file.ContentLength;

                byte[] data = new byte[] { };
                using (var binaryReader = new BinaryReader(file.InputStream))
                {
                    data = binaryReader.ReadBytes(file.ContentLength);
                }

                Document doc = new Document()
                {
                    FileName = fileName,
                    ContentType = contentType,
                    ContentLength = contentLength,
                    Data = data
                };

                db.Documents.Add(doc);
                db.SaveChanges();
            }
        }
    }
}