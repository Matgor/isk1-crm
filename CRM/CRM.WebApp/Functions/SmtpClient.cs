﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net.Mail;

namespace CRM.WebApp.Functions
{
    public class SmtpClient
    {
        public const string _service = "smtp.gmail.com";
        public const string _username = "crm.wzim.sggw@gmail.com";
        public const string _password = "Sggw100%";

        public System.Net.Mail.SmtpClient client;

        public SmtpClient()
        {
            this.client = new System.Net.Mail.SmtpClient()
            {
                Host = _service,
                Credentials = new System.Net.NetworkCredential(_username, _password),
                Port = 587,
                Timeout = 10000,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                EnableSsl = true,
            };
        }

        public MailMessage PrepareMessage(string email, string subject, string body)
        {
            MailMessage message = new MailMessage();

            message.From = new MailAddress(_username);
            message.To.Add(email);

            message.Subject = subject;
            message.Body = body;

            return message;
        }

        public void SendMessage(MailMessage message)
        {
            client.Send(message);
        }
    }
}