﻿using CRM.WebApp.Models.Clients;
using System;

namespace CRM.WebApp.Functions
{
    public static class ClientParsers
    {
        /// <summary>
        /// Client type prefix.
        /// </summary>
        const int prefixLength = 1;

        /// <summary>
        /// Parses client Id and its type.
        /// Client id should remain in format: t{i}+, where 't' stands for clients type and 'i' for actual client id. 
        /// </summary>
        /// <param name="id">Raw client id.</param>
        /// <param name="clientId">Id</param>
        /// <param name="type">Type of client</param>
        /// <returns>Parse result</returns>
        public static bool ParseClientIdAndType(string id, out int clientId, out Type type)
        {
            // Client type
            bool typeParsingResult = ParseClientType(id, out type);

            // Id
            bool idParsingResult = ParseClientId(id, out clientId);

            if (idParsingResult && typeParsingResult)
                return true;
            else
            {
                clientId = 0;
                type = null;
                return false;
            }
        }


        /// <summary>
        /// Parses client Id.
        /// Client id should remain in format: t{i}+, where 't' stands for clients type and 'i' for actual client id. 
        /// </summary>
        /// <param name="id">Raw client id.</param>
        /// <param name="clientId">Id</param>
        /// <returns>Parse result</returns>
        public static bool ParseClientId(string id, out int clientId)
        {
            try
            {
                string raw_id = id.Substring(prefixLength);
                clientId = int.Parse(raw_id);
                return true;
            }
            catch (Exception)
            {
                clientId = 0;
                return false;
            }
        }

        /// <summary>
        /// Parses client's type.
        /// </summary>
        /// <param name="id">Raw client id.</param>
        /// <param name="type">Type of client</param>
        /// <returns>Parse result</returns>
        public static bool ParseClientType(string id, out Type type)
        {
            try
            {
                char c_type = id[0];
                if (c_type == 'c')
                    type = typeof(Company);
                else if (c_type == 'p')
                    type = typeof(Person);
                else
                    throw new Exception("Parsing failed.");

                return true;
            }
            catch (Exception)
            {
                type = null;
                return false;
            }
        }
    }
}