﻿using System;
using System.Web;
using System.Web.Mvc;

using CRM.WebApp.Mixins;

namespace CRM.WebApp.Infrastructure
{
    public enum NotificationType
    {
        Success = 0,
        Info = 1,
        Warning = 2,
        Danger = 3,
    }

    public static class ControllerExt
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="controller"></param>
        /// <param name="type"></param>
        /// <param name="message"></param>
        /// <param name="showAfterRedirect"></param>
        public static void ShowNotification(this Controller controller, NotificationType type, string message, bool showAfterRedirect = false)
        {
            var notificationTypeKey = type.ToString();
            if (showAfterRedirect)
            {
                controller.TempData[notificationTypeKey] = message;
            }
            else
            {
                controller.ViewData[notificationTypeKey] = message;
            }
        }
    }

    public static class HtmlHelperExtension
    {
        public static HtmlString RenderNotifications(this HtmlHelper helper)
        {
            var notifications = string.Empty;

            foreach (var notificationType in Enum.GetNames(typeof(NotificationType)))
            {
                object notification;

                // TODO dodać view data
                if (helper.ViewContext.ViewData.ContainsKey(notificationType))
                {
                    notification = helper.ViewContext.ViewData[notificationType];
                }
                else if (helper.ViewContext.TempData.ContainsKey(notificationType))
                {
                    notification = helper.ViewContext.TempData[notificationType];
                }
                else
                {
                    notification = null;
                }

                if (!notification.IsNull())
                {
                    var notificationBoxBuilder = new TagBuilder("div");
                    notificationBoxBuilder.AddCssClass(String.Format("notification alert-{0}", notificationType.ToLowerInvariant()));
                    string innerText = "<img src=\"" + @"http://upload.wikimedia.org/wikipedia/commons/b/b6/Chrome_close_button.png" + "\" alt=\"Close button\"  class=\"notif-exit-btn\"/>" + notification.ToString();
                    notificationBoxBuilder.InnerHtml = innerText;
                    notifications += notificationBoxBuilder.ToString();
                }
            }
            return MvcHtmlString.Create(notifications);
        }
    }
}