﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CRM.WebApp.Mixins
{
    public static class StringExt
    {
        /// <summary>
        /// Checks string object existance.
        /// </summary>
        /// <param name="target">Compared string object.</param>
        /// <returns>Returns true if reference to an string object is set to null.</returns>
        public static bool IsNull(this string target)
        {
            return target == null;
        }
    }
}