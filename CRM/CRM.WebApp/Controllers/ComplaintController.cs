﻿using CRM.WebApp.Models.Applications;
using CRM.WebApp.Mixins;
using CRM.WebApp.Models;
using CRM.WebApp.Infrastructure;
using CRM.WebApp.Infrastructure;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;

namespace CRM.WebApp.Controllers
{
    public class ComplaintController : Controller
    {
        /// <summary>
        /// Data access object
        /// </summary>
        public CrmDbContext _db;

        /// <summary>
        /// If you don't know where you should redirect, use this field.
        /// </summary>
        public static string DefaultRedirectAction { get; set; }

        /// <summary>
        /// Constructor
        /// </summary>
        public ComplaintController()
        {
            // Initilize variables 

            if (_db == null)
            {
                _db = new CrmDbContext();
            }

            DefaultRedirectAction = "List";
        }

        //
        // GET: /Complaints/
        public ActionResult Index()
        {
            return RedirectToAction(DefaultRedirectAction);
        }

        //
        // GET: /Complaints/Create
        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Complaints/Create
        [HttpPost]
        public ActionResult Create(Complaint complaint)
        {
            if (ModelState.IsValid)
            {
                // Set complaints creation time to this moment.
                complaint.CreationTime = DateTime.Now;

                _db.Complaints.Add(complaint);
                _db.SaveChanges();

                this.ShowNotification(NotificationType.Success, "Reklamacja została pozytywnie dodana.", true);
                return RedirectToAction("List");
            }

            this.ShowNotification(NotificationType.Warning, "Coś poszło nie tak. Reklamacja nie została dodana.");
            return View();
        }

        // GET: /Complaints/List
        public ActionResult List()
        {
            if (_db.IsNull())
                throw new NullReferenceException("Database context is null!");

            IEnumerable<Complaint> complaints = _db.Complaints;

            if (complaints.Count() == 0)
                this.ShowNotification(NotificationType.Info, "Nie dodano jeszcze żadnych reklamacji.");

            return View(complaints);
        }

        //
        // GET: /Complaints/Edit/5
        public ActionResult Edit(int id)
        {
            Complaint complaint = null;
            complaint = _db.Complaints.First(r => r.ComplaintId == id);
            return View(complaint);
        }

        //
        // POST: /Complaints/Edit
        [HttpPost]
        public ActionResult Edit(Complaint complaint)
        {
            if (ModelState.IsValid)
            {
                _db.Entry(complaint).State = EntityState.Modified;
                _db.SaveChanges();

                this.ShowNotification(NotificationType.Success, "Reklamacja została zmieniona.", true);

                // Redirect to service request details view
                return RedirectToAction("Details", new { id = complaint.ComplaintId });
            }

            this.ShowNotification(NotificationType.Warning, "Coś poszło nie tak. Reklamacja nie została zmieniona.");

            // If received request is not valid, show edit view again for necessery corrections.
            return View(complaint);
        }

        //
        // GET: /Complaints/Details/5
        public ActionResult Details(int id)
        {
            RedirectToRouteResult redirectResult;
            Complaint complaint = FindComplaintInDb(id, out redirectResult);

            if (!redirectResult.IsNull())
            {
                this.ShowNotification(NotificationType.Warning, "Szukana reklamacja nie została odnaleziona.");
                return redirectResult;
            }
            else
                return View(complaint);
        }

        public ActionResult Delete(int id)
        {
            if (_db.IsNull())
                throw new NullReferenceException("Database context is null!");

            RedirectToRouteResult redirectResult;
            Complaint complaint = FindComplaintInDb(id, out redirectResult);

            if (!redirectResult.IsNull())
            {
                this.ShowNotification(NotificationType.Danger, "Nie można było usunąć danej reklamacji", true);
                return redirectResult;
            }

            // Remove complaint
            _db.Complaints.Remove(complaint);

            // Sync with db
            _db.SaveChanges();

            this.ShowNotification(NotificationType.Success, "Reklamacja została usunięta.", true);

            return RedirectToAction("List");
        }

        /// <summary>
        /// Help method for redirecting when seeked complaint is not found in data storage. Logs down appropriate warn log and redirects to the given action.
        /// </summary>
        /// <param name="id">Seeked complaint id.</param>
        /// <param name="action">Action where will be redirected to.</param>
        private RedirectToRouteResult ComplaintNotFoundRedirect(int id, string action)
        {
            Log.Warn(string.Format("Complaint with Id={0} is was not found and caused the exception. Redirecting to {1}", id, action), this);
            return RedirectToAction(action);
        }

        /// <summary>
        /// Searches database for complaint with same Id.
        /// If complaint isn't found, logs down error and redirects.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="redirectResult"></param>
        /// <returns></returns>
        private Complaint FindComplaintInDb(int id, out RedirectToRouteResult redirectResult)
        {
            Complaint complaint = null;
            redirectResult = null;

            try
            {
                complaint = _db.Complaints.First(c => c.ComplaintId == id);
            }
            catch (InvalidOperationException)
            {
                redirectResult = ComplaintNotFoundRedirect(id, DefaultRedirectAction);
            }

            if (redirectResult.IsNull() && !complaint.IsNull())
                Log.Info(string.Format("Found complaint: {0}", complaint.ToString()), this);

            return complaint;
        }
    }
}