﻿using CRM.WebApp.Functions;
using CRM.WebApp.Models;
using CRM.WebApp.Services.Functions.Contracts;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using PagedList;

namespace CRM.WebApp.Controllers
{
    public class MailController : Controller
    {
        public IImapClientSrv _imapClientSrv;
        public ISmtpClientSrv _smtpClientSrv;

        public MailController(IImapClientSrv imapClientSrv, ISmtpClientSrv smtpClientSrv)
        {
            _imapClientSrv = imapClientSrv;
            _smtpClientSrv = smtpClientSrv;
        }

        //
        // GET: /Mail/
        public ActionResult Index()
        {
            return RedirectToAction("List");
        }

        //
        // GET: /Mail/List
        public ActionResult List(int? page)
        {
            IEnumerable<Mail> mails = _imapClientSrv.Mails;

            mails = mails.OrderByDescending(x => x.MailId);

            var pageSize = 10;
            var pageNumber = (page ?? 1);

            return View(mails.ToPagedList(pageNumber,pageSize));
        }

        //
        // GET: /Mail/Refresh
        public ActionResult Refresh()
        {
            foreach (var mail in _imapClientSrv.GetMissingMessages())
            {
                _imapClientSrv.AddMail(mail.Key, mail.Value);
            }

            return RedirectToAction("List");
        }

        //
        // GET: /Mail/Create
        [HttpGet]
        public ActionResult Create()
        {
            MailMessage msg = new MailMessage();

            return View(msg);
        }

        //
        // POST: /Mail/Create
        [HttpPost]
        public ActionResult Create(string email, string subject, string body)
        {
            _smtpClientSrv.SendMessage(email, subject, body);

            return RedirectToAction("List");
        }

        //
        // GET: /Mail/Details/5
        [HttpGet]
        public ActionResult Details(int? id)
        {
            if (id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            Mail mail = _imapClientSrv.FindMessage((int)id);

            if (mail == null)
                return HttpNotFound();

            _imapClientSrv.SetSeenFlag((uint)mail.MailUintId);

            return View(mail);
        }

        //
        // GET: /Mail/Delete
        [HttpGet]
        public ActionResult Delete(int? id)
        {
            _imapClientSrv.RemoveMail((int)id);

            return RedirectToAction("Refresh");
        }
    }
}