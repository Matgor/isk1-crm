﻿using System.Collections.Generic;
using System.Web;

using CRM.WebApp.Models.Applications;
using CRM.WebApp.Models.Clients;

namespace CRM.WebApp.ViewModel
{
    public class DetailsServiceRequestVM
    {
        public ServiceRequest ServiceRequest { get; set; }
        public IClient Client { get; set; }
    }
}