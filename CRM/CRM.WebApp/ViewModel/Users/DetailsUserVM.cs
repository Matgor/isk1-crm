﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace CRM.WebApp.ViewModel.Users
{
    public class DetailsUserVM : AbstractUserVM
    {
        [Required]
        [DisplayName("Administator")]
        public bool IsAdmin { get; set; }
    }
}