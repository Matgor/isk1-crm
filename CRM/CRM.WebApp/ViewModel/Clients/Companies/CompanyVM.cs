﻿using CRM.WebApp.Models.Clients;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CRM.WebApp.ViewModel.Clients.Companies
{
    public class CompanyVM
    {
        public Company Company { get; set; }
        public Address Address { get; set; }
        public ContactDetails ContactDetails { get; set; }
    }
}