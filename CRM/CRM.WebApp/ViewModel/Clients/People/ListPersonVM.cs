﻿using CRM.WebApp.Models.Clients;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CRM.WebApp.ViewModel.Clients.People
{
    public class ListPersonVM : PersonVM
    {        
        public int ServiceRequestNumber { get; set; }
        //public int Complaints { get; set; }
        public bool Status { get; set; }
    }
}