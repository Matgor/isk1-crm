﻿using CRM.WebApp.Models.Clients;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CRM.WebApp.ViewModel.Clients.People
{
    public class PersonVM
    {
        public Person Person { get; set; }
        public Address Address { get; set; }
        public ContactDetails ContactDetails { get; set; }      
    }
}