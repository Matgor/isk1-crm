﻿using CRM.WebApp.Models;
using CRM.WebApp.Services.Functions.Contracts;
using S22.Imap;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Web;

namespace CRM.WebApp.Services.Functions
{
    public class ImapClientSrv : IImapClientSrv
    {
        /// <summary>
        /// Credencials for gmail server
        /// </summary>
        public const string _service = "imap.gmail.com";
        public const string _username = "crm.wzim.sggw@gmail.com";
        public const string _password = "Sggw100%";

        ICrmDbContext _dbContext;

        /// <summary>
        /// Imap client form s22 library
        /// </summary>
        private ImapClient _client;

        /// <summary>
        /// Constructor with configuring imap connection
        /// </summary>
        /// <param name="mailContext"></param>
        public ImapClientSrv(ICrmDbContext dbContext)
        {
            _client = new ImapClient
                (_service, 993, _username, _password, AuthMethod.Login, true);

            _dbContext = dbContext;
        }

        /// <summary>
        /// Add mail message to database
        /// </summary>
        /// <param name="UId">UId of message that maches on on server</param>
        /// <param name="mailMessage">Whole mail message</param>
        /// <returns>Done or not</returns>
        public bool AddMail(uint UId, MailMessage mailMessage)
        {
            try
            {
                if (mailMessage == null) throw new ArgumentNullException("Mail is null");

                _dbContext.Mails.Add(
                    new Mail()
                    {
                        MailUintId = (int)UId,
                        From = mailMessage.From.Address,
                        Subject = mailMessage.Subject,
                        Body = mailMessage.Body
                    });

                _dbContext.SaveChanges();

                return true;
            }
            catch (Exception)
            {
                return false;
            }

        }

        /// <summary>
        /// Downloads missing mail ids form gmail server
        /// </summary>
        /// <returns>List of uints</returns>
        public IEnumerable<uint> GetMissingIds()
        {
            IEnumerable<uint> allUIds = _client.Search(SearchCondition.All());

            List<uint> missingUIds = new List<uint>();

            foreach (var id in allUIds)
            {
                if (!(_dbContext.Mails.Where(x => x.MailUintId == id).Count() > 0))
                {
                    missingUIds.Add(id);
                }
            }

            return missingUIds;
        }

        /// <summary>
        /// Downloads missing messeges without attachments
        /// </summary>
        /// <returns>Dictionary keys are uints form server values are MailMessages</returns>
        public IDictionary<uint, MailMessage> GetMissingMessages()
        {
            IEnumerable<uint> missingMsgUIds = GetMissingIds();

            Dictionary<uint, MailMessage> missingMailMessages = new Dictionary<uint, MailMessage>();

            foreach (var id in missingMsgUIds)
            {
                missingMailMessages.Add(id, _client.GetMessage(id, FetchOptions.NoAttachments));
            }

            return missingMailMessages;
        }

        /// <summary>
        /// Set seen flag on server
        /// </summary>
        /// <param name="UId">UId of message</param>
        /// <returns>Done or not</returns>
        public bool SetSeenFlag(uint UId)
        {
            try
            {
                _client.SetMessageFlags(UId, "INBOX", MessageFlag.Seen);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool RemoveMail(int id)
        {
            try
            {
                Mail mail = FindMessage(id);

                _dbContext.Mails.Remove(mail);

                _dbContext.SaveChanges();

                _client.DeleteMessage((uint)mail.MailUintId);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Finds message in database
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Mail FindMessage(int id)
        {
            return _dbContext.Mails.Find(id);
        }

        public Mail[] Mails
        {
            get { return _dbContext.Mails.ToArray(); }
        }



    }
}