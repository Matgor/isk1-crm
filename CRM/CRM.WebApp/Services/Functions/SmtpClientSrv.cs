﻿using CRM.WebApp.Services.Functions.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Web;

namespace CRM.WebApp.Services.Functions
{
    public class SmtpClientSrv : ISmtpClientSrv
    {
        /// <summary>
        /// Credencials for gmail server
        /// </summary>
        public const String _service = "smtp.gmail.com";
        public const String _username = "crm.wzim.sggw@gmail.com";
        public const String _password = "Sggw100%";

        /// <summary>
        /// Build-in SmptClient
        /// </summary>
        public SmtpClient _client;

        /// <summary>
        /// Constructor with configuration of client
        /// </summary>
        public SmtpClientSrv()
        {
            this._client = new SmtpClient()
            {
                Host = _service,
                Credentials = new System.Net.NetworkCredential(_username, _password),
                Port = 587,
                Timeout = 10000,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                EnableSsl = true,
            };
        }

        /// <summary>
        /// Method for sending emails
        /// </summary>
        /// <param name="email">E-mail addres name@domain.com</param>
        /// <param name="subject">E-mail subject</param>
        /// <param name="body">Email content</param>
        /// <returns></returns>
        public bool SendMessage(string email, string subject, string body)
        {
            MailMessage message = new MailMessage();

            message.From = new MailAddress(_username);
            message.To.Add(email);

            message.Subject = subject;
            message.Body = body;

            try
            {
                _client.Send(message);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}