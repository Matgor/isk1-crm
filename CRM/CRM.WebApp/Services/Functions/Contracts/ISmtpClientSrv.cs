﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRM.WebApp.Services.Functions.Contracts
{
    public interface ISmtpClientSrv
    {
        bool SendMessage(String email, String subject, String body);
    }
}
