﻿using CRM.WebApp.Models.Applications;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace CRM.WebApp.Services.Functions.Contracts
{
    public interface IFileHandlerSrv
    {
        bool AddDocument(HttpPostedFileBase document);
        bool AddDocument(Document document);
        Document ConvertToDocument(HttpPostedFileBase file);
        Document[] Documents { get; }
        Document FindDocument(int id);
        IQueryable<Document> GetDocument(Expression<Func<Document, bool>> expression);
        bool RemoveDocumet(int id);
    }
}
