﻿using CRM.WebApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Web;

namespace CRM.WebApp.Services.Functions.Contracts
{
    public interface IImapClientSrv
    {
        bool AddMail(uint UId, MailMessage mailMessage);
        Mail FindMessage(int id);
        IEnumerable<uint> GetMissingIds();
        IDictionary<uint, MailMessage> GetMissingMessages();
        bool SetSeenFlag(uint UId);
        bool RemoveMail(int id);
        Mail[] Mails { get; }
    }
}