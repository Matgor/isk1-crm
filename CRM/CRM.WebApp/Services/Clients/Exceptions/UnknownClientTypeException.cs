﻿using CRM.WebApp.Models.Clients;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CRM.WebApp.Services.Clients
{
    /// <summary>
    /// 
    /// </summary>
    public class UnknownClientTypeException : Exception
    {
        public UnknownClientTypeException(IClient client)
            : this(client.GetType()) { }

        public UnknownClientTypeException(Type type)
        {
            ClientType = type;
        }

        public Type ClientType { get; private set; }

    }
}