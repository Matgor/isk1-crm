﻿using CRM.WebApp.Models.Clients;
using System;
using System.Linq;
using System.Linq.Expressions;

namespace CRM.WebApp.Services.Clients
{
    public interface ICompaniesSrv : IClientSrv
    {
        Company FindCompany(int id);
        IQueryable<Company> GetCompanies(Expression<Func<Company, bool>> expression);
        Company[] Companies { get; }
        bool RemoveCompany(int id);
    }
}
