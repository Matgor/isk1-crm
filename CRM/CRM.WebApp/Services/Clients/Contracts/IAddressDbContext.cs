﻿using CRM.WebApp.Models.Clients;
using System.Data.Entity;

namespace CRM.WebApp.Services.Clients
{
    public interface IAddressDbContext : IDbContext
    {
        IDbSet<Address> Addresses { get; set; }
    }
}
