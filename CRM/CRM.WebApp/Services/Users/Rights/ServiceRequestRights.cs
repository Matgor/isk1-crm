﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CRM.WebApp.Services.Users.Rights
{
    [Flags]
    public enum ServiceRequestRights
    {
        CreateRequest = 1,
        EditRequest = 2,
        ShowRequest = 4,
        DeleteRequest = 8,
        ChangeStatus = 16,
        FinishRequest = 32
    }
}