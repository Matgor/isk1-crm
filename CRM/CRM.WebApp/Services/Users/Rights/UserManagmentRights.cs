﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CRM.WebApp.Services.Users.Rights
{
    [Flags]
    public enum UserManagmentRights
    {
        CreateUser = 1,
        EditMyProfile = 2,
        EditEveryProfile = 4,
        RemoveUser = 8,
        SetUserRights = 16,
        ShowMyProfile = 32,
        ShowEveryProfile = 64
    }
}