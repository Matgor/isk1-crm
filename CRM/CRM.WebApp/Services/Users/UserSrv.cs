﻿using System;
using System.Linq;
using System.Linq.Expressions;

using CRM.WebApp.Models;
using CRM.WebApp.Models.Users;
using CRM.WebApp.Services.ServiceRequests.Contracts;

namespace CRM.WebApp.Services.Users
{
    public class UserSrv : IUsersSrv
    {
        /// <summary>
        /// Users database context.
        /// </summary>
        ICrmDbContext _dbContext;

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="usersCntx"></param>
        public UserSrv(ICrmDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        /// <summary>
        /// Adds user to the repository.
        /// </summary>
        /// <param name="userToAdd">User to add.</param>
        /// <returns>Added user. If operation was unsucessful, returns null.</returns>
        public User Add(User userToAdd)
        {
            if (userToAdd == null) throw new ArgumentNullException("User to add is null.");
            User addedUser = null;
            try
            {
                addedUser = _dbContext.Users.Add(userToAdd);
                _dbContext.SaveChanges();
            }
            catch (Exception e) { }
            return addedUser;
        }

        /// <summary>
        /// Rewrites changes from given user.
        /// </summary>
        /// <param name="modifiedUser">User with modifications.</param>
        /// <returns>Returns true if editing was successful. False otherwise.</returns>
        public bool Edit(User modifiedUser)
        {
            if (modifiedUser == null) throw new ArgumentNullException("Modified user is null.");

            bool editing_result = false;
            try
            {
                User originalUser = Find(modifiedUser.UserId);
                if (originalUser == null)
                    new NullReferenceException("User with this ID does not exist in repository.");

                var entry = _dbContext.Entry(originalUser);

                // Rewrite the old password so it won't change in database
                // For changing password, use another method.
                modifiedUser.Password = originalUser.Password;

                entry.CurrentValues.SetValues(modifiedUser); // TODO should be tested
                _dbContext.SaveChanges();
                editing_result = true;
            }
            catch (Exception e) { }

            return editing_result;
        }


        /// <summary>
        /// Changes user password.
        /// </summary>
        /// <param name="userId">User ID</param>
        /// <param name="string">New password.</param>
        /// <returns>Was changing password successful.</returns>
        public bool ChangePassword(int userId, string oldPassword, string newPassword)
        {
            bool result = false;

            var user = Find(userId);
            if (user.Password == oldPassword)
            {
                user.Password = newPassword;
                _dbContext.SaveChanges();
                result = true;
            }

            return result;
        }

        /// <summary>
        /// Looks for the user with specified ID in the repository and returns its instance.
        /// </summary>
        /// <param name="id">Looked up user ID.</param>
        /// <returns>User instance. Null when the user was not found.</returns>
        public User Find(int id)
        {
            User user;
            try
            {
                var query = from u in _dbContext.Users
                            where u.UserId == id
                            select u;
                user = query.Single();
            }
            catch (Exception) { user = null; }
            return user;
        }

        /// <summary>
        /// Removes user with specified ID from the repository.
        /// </summary>
        /// <param name="id">ID of the user to remove.</param>
        /// <returns>Returns true if user removing was successful. False otherwise.</returns>
        public bool Remove(int id)
        {
            bool removing_result = false;
            try
            {
                User userToRemove = Find(id);
                _dbContext.Users.Remove(userToRemove);
                _dbContext.SaveChanges();
                removing_result = true;
            }
            catch (Exception) { }

            return removing_result;
        }

        /// <summary>
        /// Gets set of users basing on predicate.
        /// </summary>
        /// <param name="predicate">Filter.</param>
        /// <returns>Queryable collection of users.</returns>
        public IQueryable<User> GetUsers(Expression<Func<User, bool>> predicate)
        {
            return _dbContext.Users.Where(predicate);
        }

        /// <summary>
        /// Gets a single user from database basing on predicate.
        /// </summary>
        /// <param name="predicate">Filter.</param>
        /// <returns>User instance.</returns>
        public User GetUser(Expression<Func<User, bool>> predicate)
        {
            User user = null;
            try
            {
                user = _dbContext.Users.Single<User>(predicate);
            }
            catch (InvalidOperationException) { }
            return user;
        }

        /// <summary>
        /// Retrives all user instances from repository.
        /// </summary>
        public User[] Users
        {
            get { return _dbContext.Users.ToArray(); }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <typeparam name="Y"></typeparam>
        /// <param name="userName"></param>
        /// <param name="viewHistoryItem"></param>
        public void UpdateUserViewHistory<T, Y>(string userName, Y viewHistoryItem)
        {
            if (viewHistoryItem == null) throw new ArgumentNullException("view history item is null!");
            var user = GetUser(u => u.Login == userName) as IViewHistoryKeeper<T, Y>;
            if (user == null) return;
            user.Update(viewHistoryItem);
            _dbContext.SaveChanges();
        }
    }
}