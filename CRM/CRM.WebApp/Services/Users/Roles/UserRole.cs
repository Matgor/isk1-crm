﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using CRM.WebApp.Services.Users.Rights;

namespace CRM.WebApp.Services.Users.Roles
{
    public abstract class UserRole
    {
        public abstract UserManagmentRights UserManagmentRights { get; }
        public abstract ServiceRequestRights ServiceRequestRights { get; }

    }
}
