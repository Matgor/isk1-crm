﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CRM.WebApp.Services.ServiceRequests.Contracts;

namespace CRM.WebApp.Services.Clients
{
    public class EditingResult : IEditingResult
    {
        public EditingResult()
        {
            ChangedPropertyNames = new HashSet<string>();
        }

        public bool RequestWasChanged { get; set; }
        public bool Successful { get; set; }
        public int ChangedPropertiesCount { get { return ChangedPropertyNames.Count; } }
        public ISet<string> ChangedPropertyNames { get; set; }
    }
}