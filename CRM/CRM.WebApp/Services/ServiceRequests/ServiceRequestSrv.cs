﻿using CRM.WebApp.Models;
using CRM.WebApp.Models.Applications;
using CRM.WebApp.Services.ServiceRequests.Contracts;
using CRM.WebApp.Mixins;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Data.Entity;
using CRM.WebApp.Models.Applications.History;
using CRM.WebApp.Services.Clients;
using CRM.WebApp.Services.Functions.Contracts;
using CRM.WebApp.Services.Clients.Exceptions;
using CRM.WebApp.Models.Clients;

namespace CRM.WebApp.Services.ServiceRequests
{
    public class ServiceRequestSrv : IServiceRequestSrv
    {
        /// <summary>
        /// Database context.
        /// </summary>
        ICrmDbContext _dbContext;

        /// <summary>
        /// Clients service.
        /// </summary>
        IClientSrv _clientSrv;

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="serviceRequestsCntx"></param>
        public ServiceRequestSrv(ICrmDbContext dbCntx, IClientSrv clientSrv)
        {
            // ServiceRequests Context
            if (dbCntx == null) throw new ArgumentNullException("Database context is null");
            else _dbContext = dbCntx;

            // Clients Server
            if (clientSrv == null) throw new ArgumentNullException("clientSrv is null");
            else _clientSrv = clientSrv;

        }


        /// <summary>
        /// Adds new service request to repository.
        /// </summary>
        /// <param name="serviceRequest">Added service request. If request was not added, returns null.</param>
        /// <returns></returns>
        public ServiceRequest Add(ServiceRequest serviceRequest, string clientId)
        {
            if (serviceRequest == null) throw new ArgumentNullException("Service request is null.");

            if (string.IsNullOrEmpty(clientId)) throw new Exception("Client Id is null or empty.");

            // Prepare request
            var now = DateTime.Now;
            serviceRequest.CreationTime = now;

            // Add history arg to request
            var historyItem = new RequestCreate
            {
                CreationTime = now,
            };

            // Get all modified fields
            ISet<string> changedFields;
            {
                ServiceRequest emptyRequest = new ServiceRequest();
                changedFields = emptyRequest.GetChangedFields(serviceRequest, typeof(IncomparableField), typeof(IDescribable)); // order matters!
                // Add fields with non-empty Description. If description is empty or doesn't exist, the field was not modified.
                serviceRequest.GetType().GetProperties().Where(p => p.PropertyType.GetInterfaces().Contains(typeof(IDescribable)) && !string.IsNullOrEmpty((p.GetValue(serviceRequest) as IDescribable).Description)).ToList().ForEach((f) => { changedFields.Add(f.Name); });
            }

            serviceRequest.AddHistoryItem(historyItem, true);

            // Add request to database collection
            var addedServiceRequest = _dbContext.ServiceRequests.Add(serviceRequest);

            var client = _clientSrv.FindClient(clientId);
            // Assign request to client
            // Check is not already assigned
            if (client.ServiceRequests != null && client.ServiceRequests.Where(x => x.ServiceRequestId == serviceRequest.ServiceRequestId).Count() > 0)
            {
                throw new Exception("This client already contains service request with such an Id");
            }
            client.ServiceRequests.Add(serviceRequest);

            // Save changes in repository
            _dbContext.SaveChanges();

            return addedServiceRequest;
        }

        /// <summary>
        /// Edits existing service request in database and creates apropriate history.
        /// </summary>
        /// <param name="modifiedServiceRequest">Service request with modified values.</param>
        /// <returns>Editing result.</returns>
        public IEditingResult Edit(ServiceRequest modifiedServiceRequest)
        {
            IEditingResult editingResult = new EditingResult();
            editingResult.Successful = false;

            /*
             * Compare model with actual model in database, then rewrite modifications and set all changed properties on the model as modified.
             * And don't forget about adding history items about what have chenged.
             * */
            int request_id = modifiedServiceRequest.ServiceRequestId;

            // Get original request
            ServiceRequest originalRequest = (from r in _dbContext.ServiceRequests where r.ServiceRequestId == request_id select r).SingleOrDefault();
            editingResult.ChangedPropertyNames = modifiedServiceRequest.GetChangedFields(originalRequest, typeof(IncomparableField));
            if (editingResult.ChangedPropertiesCount > 0)
            {
                var requestEntry = _dbContext.Entry(originalRequest);

                // Set changed entries as modified so the changes shall affect.
                {
                    //requestEntry.State = EntityState.Modified;
                    requestEntry.CurrentValues.SetValues(modifiedServiceRequest);

                    // Compare again, some more complex properties, like Analyze which contains additional fields, might have not been copied.
                    ISet<string> complexPropertiesNames = modifiedServiceRequest.GetChangedFields(originalRequest, typeof(IncomparableField));

                    // For more complex properties
                    foreach (string propertyName in complexPropertiesNames)
                    {
                        object originalProperty = originalRequest.GetPropValue(propertyName);
                        var originalPropertyEntry = _dbContext.Entry(originalProperty);
                        originalPropertyEntry.CurrentValues.SetValues(modifiedServiceRequest.GetPropValue(propertyName));
                        originalPropertyEntry.State = EntityState.Modified;
                    }
                }

                // Add history arg
                {
                    RequestHistoryItem historyItem;
                    if (editingResult.ChangedPropertiesCount == 1)
                    {
                        // Add single RequestFieldChange
                        historyItem = new RequestFieldChange()
                        {
                            ChangedFieldName = editingResult.ChangedPropertyNames.First(),
                            CreationTime = DateTime.Now
                        };
                        _dbContext.RequestFieldChangeItems.Add(historyItem as RequestFieldChange);
                    }
                    else // count > 1
                    {
                        // Add a mutiple fields as single RequestFieldsChange
                        historyItem = new RequestFieldsChange() { CreationTime = DateTime.Now };
                        foreach (string propertyName in editingResult.ChangedPropertyNames)
                        {
                            var changedFieldItem = new RequestFieldChange() { ChangedFieldName = propertyName, CreationTime = DateTime.Now };
                            (historyItem as RequestFieldsChange).ChangedFields.Add(changedFieldItem);
                        }
                        _dbContext.RequestFieldsChangeItems.Add(historyItem as RequestFieldsChange);
                    }

                    originalRequest.History.Items.Add(historyItem);
                }

                _dbContext.SaveChanges();
                editingResult.Successful = true;
            }
            else
            {
                editingResult.Successful = true;
            }

            return editingResult;
        }

        /// <summary>
        /// Looks up repository for service request with specified ID.
        /// </summary>
        /// <param name="id">Service request ID.</param>
        /// <returns>Seeked service request. If request with such ID doesn't exist in repository, returns null.</returns>
        public ServiceRequest Find(int id)
        {
            return _dbContext.ServiceRequests.Find(id);
        }


        /// <summary>
        /// Removes service request from the repository.
        /// </summary>
        /// <param name="id">Service request ID.</param>
        /// <returns>Was removal successful.</returns>
        public bool Remove(int id)
        {
            bool removing_result = false;
            try
            {
                var query = from r in _dbContext.ServiceRequests where r.ServiceRequestId == id select r;
                ServiceRequest request = query.Single();
                _dbContext.ServiceRequests.Remove(request);
                _dbContext.SaveChanges();
                removing_result = true;
            }
            catch (Exception) { }
            return removing_result;
        }

        /// <summary>
        /// Get service requests from repository according to predicate.
        /// </summary>
        /// <param name="expression">Predicate</param>
        /// <returns>Set of service requests.</returns>
        public IQueryable<ServiceRequest> GetServiceRequests(Expression<Func<ServiceRequest, bool>> expression)
        {
            if (expression == null) throw new ArgumentNullException("expression is null");

            return _dbContext.ServiceRequests.Where(expression);
        }

        /// <summary>
        /// Gets all service requests from repository.
        /// </summary>
        public ServiceRequest[] ServiceRequests
        {
            get { return _dbContext.ServiceRequests.ToArray(); }
        }

        // TODO remove it from here
        /// <summary>
        /// Gets documents from repository.
        /// </summary>
        /// <param name="id">Document ID.</param>
        /// <returns>Document</returns>
        public Document GetDocument(int id)
        {
            return _dbContext.Documents.Find(id);
        }

        /// <summary>
        /// Searches for owner of the service request.
        /// </summary>
        /// <param name="serviceRequest">Service request.</param>
        /// <returns>Client who owns service request.</returns>
        public Models.Clients.IClient FindOwner(ServiceRequest serviceRequest)
        {
            // Search in companies
            try
            {
                return (_clientSrv as ICompaniesSrv).GetCompanies(c => c.ServiceRequests.Any(r => r.ServiceRequestId == serviceRequest.ServiceRequestId)).Single();
            }
            catch (Exception) { }

            // Search in person clients
            try
            {
                return (_clientSrv as IPeopleSrv).GetPeople(c => c.ServiceRequests.Any(r => r.ServiceRequestId == serviceRequest.ServiceRequestId)).Single();
            }
            catch (Exception) { }

            // If not found, return null
            return null;
        }
    }
}