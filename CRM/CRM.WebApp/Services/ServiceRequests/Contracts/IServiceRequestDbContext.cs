﻿using CRM.WebApp.Models.Applications;
using CRM.WebApp.Models.Applications.History;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRM.WebApp.Services.ServiceRequests.Contracts
{
    public interface IServiceRequestDbContext : IDbContext
    {
        IDbSet<ServiceRequest> ServiceRequests { get; set; }
        IDbSet<RequestCreate> RequestCreateHistoryItems { get; set; }
        IDbSet<RequestFieldChange> RequestFieldChangeItems { get; set; }
        IDbSet<RequestStatusChange> RequestStatusChangeItems { get; set; }
        IDbSet<RequestFinish> RequestFinishHistoryItems { get; set; }
        IDbSet<RequestFieldsChange> RequestFieldsChangeItems { get; set; }
    }
}
