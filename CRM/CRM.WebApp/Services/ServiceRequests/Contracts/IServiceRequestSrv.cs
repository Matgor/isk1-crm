﻿using CRM.WebApp.Models.Applications;
using CRM.WebApp.Models.Clients;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace CRM.WebApp.Services.ServiceRequests.Contracts
{
    public interface IServiceRequestSrv
    {
        ServiceRequest Add(ServiceRequest serviceRequest, string clientId = null);
        IEditingResult Edit(ServiceRequest modifiedServiceRequest);
        ServiceRequest Find(int id);
        bool Remove(int id);
        IQueryable<ServiceRequest> GetServiceRequests(Expression<Func<ServiceRequest, bool>> expression);
        ServiceRequest[] ServiceRequests { get; }
        IClient FindOwner(ServiceRequest serviceRequest);

        // TODO it shouldnt be here
        Document GetDocument(int id);

    }
}
