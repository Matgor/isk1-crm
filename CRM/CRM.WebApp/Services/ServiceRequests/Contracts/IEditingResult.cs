﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRM.WebApp.Services.ServiceRequests.Contracts
{
    public interface IEditingResult
    {
        bool RequestWasChanged { get; set; }
        bool Successful { get; set; }
        int ChangedPropertiesCount { get; }
        ISet<string> ChangedPropertyNames { get; set; }
    }
}
