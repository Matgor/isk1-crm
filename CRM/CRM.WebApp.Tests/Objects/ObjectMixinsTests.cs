﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using CRM.WebApp.Models.Applications;
using CRM.WebApp.Models.Applications.Services;
using CRM.WebApp.Mixins;

namespace CRM.WebApp.Tests.Objects
{
    [TestClass]
    public class ObjectMixinsTests
    {
        /// <summary>
        /// This tests comapres two objects with the same properties but different values. 
        /// </summary>
        [TestMethod]
        public void GetChangeFieldsTest1()
        {
            // Arange 
            #region request #1
            ServiceRequest request1 = new ServiceRequest
            {
                ServiceRequestId = 666,
                Title = "Oryginalny tytuł zgłoszenia.",
                CreationTime = new DateTime(2002, 2, 23, 16, 23, 34),
                Analyze = new Analyze
                {
                    AnalyzeId = 123,
                    Description = "Oryginalny opis analizy"
                },
                Repair = new Repair
                {
                    RepairId = 0,
                    Description = ""
                },
                PredictedRealizationDate = new DateTime(2002, 3, 5, 16, 23, 34),
                Status = ServiceRequestStatus.InProgress
            };

            #endregion

            #region request #2
            ServiceRequest request2 = new ServiceRequest
            {
                ServiceRequestId = 666,
                Title = "Oryginalny tytuł zgłoszenia.",
                CreationTime = new DateTime(2002, 2, 23, 16, 23, 34),
                Analyze = new Analyze
                {
                    AnalyzeId = 123,
                    Description = "Zmieniony opis analizy"
                },
                Repair = new Repair
                {
                    RepairId = 0,
                    Description = "Zmieniony opis naprawy."
                },
                PredictedRealizationDate = new DateTime(request1.CreationTime.Value.AddDays(14).Ticks),
                Status = ServiceRequestStatus.InProgress
            };
            #endregion

            // Act
            var changedProperties = request1.GetChangedFields(request2);

            // Assert
            Assert.AreNotEqual(request1, request2);

            Assert.AreEqual(3, changedProperties.Count);
        }

        /// <summary>
        /// This tests comapres two objects with the same properties and values. 
        /// </summary>
        [TestMethod]
        public void GetChangeFieldsTest2()
        {
            // Arange 
            #region request #1
            ServiceRequest request1 = new ServiceRequest
            {
                ServiceRequestId = 666,
                Title = "Oryginalny tytuł zgłoszenia.",
                CreationTime = new DateTime(2002, 2, 23, 16, 23, 34),
                Analyze = new Analyze
                {
                    AnalyzeId = 123,
                    Description = "Oryginalny opis analizy"
                },
                Repair = new Repair
                {
                    RepairId = 0,
                    Description = ""
                },
                PredictedRealizationDate = new DateTime(2002, 3, 5, 16, 23, 34),
                Status = ServiceRequestStatus.InProgress
            };

            #endregion

            #region request #2
            ServiceRequest request2 = new ServiceRequest
            {
                ServiceRequestId = 666,
                Title = "Oryginalny tytuł zgłoszenia.",
                CreationTime = new DateTime(2002, 2, 23, 16, 23, 34),
                Analyze = new Analyze
                {
                    AnalyzeId = 123,
                    Description = "Oryginalny opis analizy"
                },
                Repair = new Repair
                {
                    RepairId = 0,
                    Description = ""
                },
                PredictedRealizationDate = new DateTime(2002, 3, 5, 16, 23, 34),
                Status = ServiceRequestStatus.InProgress
            };
            #endregion

            // Act
            var changedProperties = request1.GetChangedFields(request2);

            // Assert
            Assert.AreEqual(0, changedProperties.Count);
        }
    }
}
